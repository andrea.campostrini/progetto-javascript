var locations = new Array();
var lat,lon,mapOptions
var layer =  new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
var map = new L.map('map', mapOptions);
function Cerca(lat, lon) {
    rimuoviRisultati();
    var via = document.getElementById("ricerca").value;
    let radius = document.getElementById("radius").value;
    if(!Number.isInteger(radius) || radius < 100 || radius > 30000) {
        radius = 5000;
    }
    var cosa = new Array();
    if(document.getElementById('Negozi').checked == true){
        cosa.push("shop");
    }
    if(document.getElementById('Caffetterie').checked == true){
        cosa.push("café");
    }
    if(document.getElementById('Banche').checked == true){
        cosa.push("bank");
    }
    if(document.getElementById('Dentisti').checked == true){
        cosa.push("dentist");
    }
    if(document.getElementById('Parcheggi').checked == true){
        cosa.push("parking");
    }
    if(document.getElementById('Parchi').checked == true){
        cosa.push("park");
    }
    if(document.getElementById('Bancomat').checked == true){
        cosa.push("cash dispenser");
    }
    if(document.getElementById('Supermercati').checked == true){
        cosa.push("supermarket");
    }
    if(document.getElementById('Parrucchieri').checked == true){
        cosa.push("hairdressers");
    }
    if(document.getElementById('Edicole').checked == true){
        cosa.push("newsagents");
    }
    if(document.getElementById('Tabaccherie').checked == true){
        cosa.push("tobacconists");
    }
    if(document.getElementById('Fiorerie').checked == true){
        cosa.push("florists");
    }
    if(cosa.length == 0) {
        cosa.push("shop");
        cosa.push("café");
        cosa.push("bank");
        cosa.push("dentist");
        cosa.push("parking");
        cosa.push("park");
        cosa.push("cash dispenser");
        cosa.push("supermarket");
        cosa.push("hairdressers");
        cosa.push("newsagents");
        cosa.push("tobacconists");
        cosa.push("florists");
    }
    var client = new XMLHttpRequest();
    client.onreadystatechange = () => {
        if(client.readyState == 4 && client.status == 200) {
            const json = JSON.parse(client.responseText);
            let b = json.results;
            for(let index = 0; index < b.length; index++) {
                var c = (json.results[index].poi.categories);
                for(let k = 0; k < cosa.length; k++){
                    if(c.includes(cosa[k])) {
                        var a = json.results[index];
                        locations.push(a);
                        console.log(a);
                    }
                }
            }
            Array.prototype.unique = function() {
                return Array.from(new Set(this));
            }
            locations = locations.unique();
            GeneraMappa(lat, lon, locations);
        }
    }
    client.open("GET", "https://api.tomtom.com/search/2/nearbySearch/.json?key=x83otDnD2EWOP5td36wyAyYGj6WkEA33&limit=50&lat=" + lat + "&lon=" + lon + "&radius=" + radius + "&countrySet=it");
    client.send();
}

function Exist() {
    var via = document.getElementById("ricerca").value;
    var exist = false;
    var road = via.split(", ")[0];
    var town = via.split(", ")[1];
    var county = via.split(", ")[2];
    var state = via.split(", ")[3];
    var client = new XMLHttpRequest();
    client.onreadystatechange = () => {
        if(client.readyState == 4 && client.status == 200) {
            var json = JSON.parse(client.responseText);
            for(let index = 0; index < json.length; index++) {
                var a = json[index].address.road;
                var b = json[index].address.town;
                var c = json[index].address.county;
                var d = json[index].address.state;
                if(a != undefined && b != undefined && c != undefined && d != undefined) {
                    if(road.toLowerCase() == a.toLowerCase() && town.toLowerCase() == b.toLowerCase() && county.toLowerCase() == c.toLowerCase() && state.toLowerCase() == d.toLowerCase()) {
                        lat = json[index].lat;
                        lon = json[index].lon;
                        Cerca(lat, lon);
                        exist = true;
                    }
                }
            }
            if(!exist) {
                GetRisultatiPossibili();
            }
        }
    }
    client.open("GET", "https://nominatim.openstreetmap.org/search?street=" + road + "&country=Italy&format=json&polygon=1&addressdetails=1")
    client.send();
}
function GetRisultatiPossibili() {
    var via = document.getElementById("ricerca").value;
    var client = new XMLHttpRequest();
    client.onreadystatechange = () => {
        if(client.readyState == 4 && client.status == 200) {
            var json = JSON.parse(client.responseText);
            var v = new Array();
            for(let index = 0; index < json.length; index++) {
                if(json[index].address.town === undefined|| json[index].address.road === undefined){
                    //niente
                } else {
                    b = json[index].address.road + ", " + json[index].address.town + ", " + json[index].address.county + ", "+ json[index].address.state;
                    v.push(b);
                }
            }
            Array.prototype.unique = function() {
                return Array.from(new Set(this));
            }
            VisualizzaRisultati(v.unique());
        }
    }
    client.open("GET", "https://nominatim.openstreetmap.org/search?street=" + via + "&country=Italy&format=json&polygon=1&addressdetails=1")
    client.send();
}

function VisualizzaRisultati(risultati) {
    var results = document.getElementById('query');
    results.innerHTML = '';
    var i = 0;
    risultati.forEach(risultato => {
        var button = document.createElement('button');
        button.className = 'risultato' + i;
        button.innerText = risultato;
        results.appendChild(button);
        i++;
    });
    window.onclick = function(evento) {
        for(var j = 0; j < risultati.length; j++)
        if(evento.target.matches('.risultato' + j)) {
            var via = document.getElementById("ricerca");
            via.value = risultati[j];
        }
    }
}

window.onclick = function(evento) {
    if(!evento.target.matches('.filtro') && !evento.target.matches('.contenitore') && !evento.target.matches('.contenuto')) {
        var contenuto = document.getElementsByClassName("contenitore");
        for(let i = 0; i < contenuto.length; i++) {
            var oggetto = contenuto[i];
            if (oggetto.classList.contains('show')) {
                oggetto.classList.remove('show');
            }
        }
    }
}

function filtra() {
    document.getElementById("contenuto-filtra").classList.toggle("show");
}

window.onclick = function(evento) {
    if(!evento.target.matches('.risultato') && !evento.target.matches('.risultati') && !evento.target.matches('.cerca')) {
        rimuoviRisultati();
    }
}
function getIP() { 
    var client = new XMLHttpRequest();
    client.onreadystatechange = () => {
        if(client.readyState == 4 && client.status == 200) {
            LocalizeIP(JSON.parse(client.responseText).ip);
        }
    }
    client.open("GET", "https://api.ipify.org/?format=json")
    client.send();
}
getIP()

 function LocalizeIP(ip){
    var client = new XMLHttpRequest();
    client.onreadystatechange = () => {
        if(client.readyState == 4 && client.status == 200) {
            var json =JSON.parse(client.responseText);
            lat = json.lat
            lon = json.lon
            GeneraMappa(lat,lon,locations)
        }
    }
    client.open("GET", "http://ip-api.com/json/"+ ip)
    client.send() 
 }
function rimuoviRisultati() {
    var contenuto = document.getElementById("query");
    contenuto.innerHTML = '';
}

function GeneraMappa(lat, lon, locations) {
    mapOptions = {
        center: [lat, lon],
        zoom: 14
    }


    map = L.map('map', mapOptions);

    layer = L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    map.addLayer(layer);
    let popupOption = {
        "closeButton": false
    }
    console.log(locations[0].position.lat)
    locations.forEach(element => {
        new L.Marker([element.position.lat, element.position.lon]).addTo(map).on("mouseover", event => {
            var popup = L.popup().setLatLng(element.position.lat, element.position.lon).setContent('<h3 class="w-16 h-16 p-4>' + element.poi.name + '</h3>').closeButton(false);
            event.target.bindPopup(pupup).openPopup();
        }).on("mouseout", event => {
            event.target.closePopup();
        })
    });
}

//se premo invio è come se avessi premuto il pulsante cerca
var ricerca = document.getElementById("ricerca");
ricerca.addEventListener("tastoPremuto", function(evento) {
    if (evento.key === "Enter") {
        evento.preventDefault();
        document.getElementById("cerca").click();
    }
});